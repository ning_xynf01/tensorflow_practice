import tensorflow as tf
# run "tensorboard --logdir=tensorflow_save" in terminal after training


input = tf.placeholder(tf.int32)
a = tf.constant(10)
b = tf.constant(5)
c = tf.constant(4)
d = tf.constant(2)
x_add = tf.placeholder(tf.int32)
x_minus = tf.placeholder(tf.int32)
x_mul = tf.placeholder(tf.int32)
x_div = tf.placeholder(tf.int32)


x_add = tf.add(input, a)
x_minus = tf.subtract(x_add, b)
x_mul = tf.multiply(x_minus, c)
x_div = tf.divide(x_mul, d)

# init_op = tf.global_variables_initializer()


with tf.Session() as sess:
    log_writer = tf.summary.FileWriter("tensorflow_save", sess.graph)

    # sess.run(init_op)
    sess.run(x_div, feed_dict={input : 100})
    # x_div.eval()
    x_mul.eval(feed_dict={input : 100})
    # sess.run(x_mul, feed_dict=tf.constant(100))
    # sess.run(x_minus, feed_dict=tf.constant(100))
    # sess.run(x_add, feed_dict=tf.constant(100))
